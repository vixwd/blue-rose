import { AbilityScore } from "../Actor/shared/Abilities";

export type TestType = {
    ability: AbilityScore;
    focus: string;
} | null;
