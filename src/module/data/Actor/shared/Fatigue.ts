export enum Fatigue {
    None = 0,
    Winded = 1,
    Fatigued = 2,
    Exhausted = 3,
    Dying = 4,
}
