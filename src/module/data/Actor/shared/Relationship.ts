export type Relationship = {
    name: string;
    intensity: number;
    bond: string;
};
