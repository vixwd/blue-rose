export type Persona = {
    drives: string[];
    goals: string[];
};

export const defaultPersona: Persona = {
    drives: [ "", "", "" ],
    goals: ["", "", ""],
};
