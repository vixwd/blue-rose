import { Abilities, defaultAbilities } from "./shared/Abilities";
import { Fatigue } from "./shared/Fatigue";
import { Persona, defaultPersona } from "./shared/Persona";
import { Relationship } from "./shared/Relationship";
import { Resource } from "./shared/Resource";

export type PlayerCharacterData = {
    level: number;

    socialClass: string;
    background: string;
    profession: string;

    abilities: Abilities;

    health: Resource;
    conviction: number;
    fatigue: Fatigue;
    toughness: number;

    speed: number;
    defense: number;
    armorRating: number;
    armorPenalty: number;

    persona: Persona;
    relationships: Map<number, Relationship>;
    notes: string;

    resources: number;
    financialCondition: number;

    items: any[];
};

export const emptyPlayerCharacter: PlayerCharacterData = {
    level: 1,

    socialClass: "",
    background: "",
    profession: "",

    abilities: defaultAbilities,

    health: { max: 20, value: 20, min: 0 },
    conviction: 0,
    fatigue: Fatigue.None,
    toughness: 0,

    speed: 10,
    defense: 10,
    armorRating: 0,
    armorPenalty: 0,

    persona: defaultPersona,
    relationships: new Map(),
    notes: "",

    resources: 0,
    financialCondition: 0,

    items: []
};
