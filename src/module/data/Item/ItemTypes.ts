export type PhysicalItemType = "Physical";
export type ShieldType = "Shield";
export type ArmorType = "Armor";
export type WeaponType = "Weapon";
export type FocusType = "Focus";
export type PowerType = "Power";
export type TalentType = "Talent";

export type ItemType = PhysicalItemType | ShieldType | ArmorType | WeaponType | FocusType | PowerType | TalentType;

export const physicalItemType: PhysicalItemType = "Physical";
export const shieldType: ShieldType = "Shield";
export const armorType: ArmorType = "Armor";
export const weaponType: WeaponType = "Weapon";
export const focusType: FocusType = "Focus";
export const powerType: PowerType = "Power";
export const talentType: TalentType = "Talent";
