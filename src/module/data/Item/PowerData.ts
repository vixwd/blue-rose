import { PowerType, powerType } from "./ItemTypes";
import { BaseItemData } from "./BaseItemData";
import { ActionType } from "../shared/ActionType";
import { TestType } from "../shared/TestType";
import { AbilityScore } from "../Actor/shared/Abilities";

export type PowerData = BaseItemData & {
    type: PowerType;
    time: ActionType;
    focus: string;
    ability: AbilityScore;
    force: number;
    fatigue: string;
};

export const powerData: PowerData = {
    type: powerType,
    description: "",
    time: ActionType.varies,
    focus: "",
    ability: "Accuracy",
    force: 10,
    fatigue: "",
};
