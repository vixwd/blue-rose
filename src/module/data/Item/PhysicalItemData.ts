import { BaseItemData } from "./BaseItemData";
import { physicalItemType } from "./ItemTypes";

export type PhysicalItemData = BaseItemData & {
    quantity: number;
    weight: number;
};

export const emptyPhysicalItem: PhysicalItemData = {
    type: physicalItemType,
    description: "",
    quantity: 1,
    weight: 0,
};
