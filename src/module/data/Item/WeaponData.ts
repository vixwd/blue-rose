import { PhysicalItemData, emptyPhysicalItem } from "./PhysicalItemData";
import { WeaponType, weaponType } from "./ItemTypes";
import { ActionType } from "../shared/ActionType";
import { RollableItemData } from "./RollableItemData";

export type WeaponData = PhysicalItemData &
    RollableItemData & {
        type: WeaponType;
        attackRoll: string;
        damage: string;
        focus: string;
        shortRange: string;
        longRange: string;
        reloadTime: ActionType;
    };

export const emptyWeapon: WeaponData = {
    ...emptyPhysicalItem,
    type: weaponType,
    defaultAbility: "",
    focus: "",
    attackRoll: "+1",
    damage: "1d6",
    shortRange: "",
    longRange: "",
    reloadTime: ActionType.none,
};
