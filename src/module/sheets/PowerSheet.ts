import { powerType } from "../data/Item/ItemTypes";

export class PowerSheet extends ItemSheet {
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["blue-rose", "sheet", powerType],
            template: "systems/blue-rose/templates/sheets/powerSheet.html",
            width: 520,
            height: 300,
        });
    }

    /** @override */
    setPosition(options = {}) {
        const position = super.setPosition(options);
        const sheetBody = (this.element as JQuery<HTMLElement>).find(".sheet-body");
        const bodyHeight = position.height - 150;
        sheetBody.css("height", bodyHeight);
        return position;
    }

    /** @override */
    activateListeners(html: JQuery) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.options.editable) return;
    }

    /** @override */
    _updateObject(_: Event, formData: any) {
        // Re-combine formData
        formData = Object.entries(formData).reduce<any>(
            (obj, e) => {
                obj[e[0]] = e[1];
                return obj;
            },
            { _id: this.object._id }
        );

        // Update the Item
        return this.object.update(formData);
    }
}
