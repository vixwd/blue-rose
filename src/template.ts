import { powerData } from "./module/data/Item/PowerData";
import { emptyArmor } from "./module/data/Item/ArmorData";
import { emptyFocus } from "./module/data/Item/FocusData";
import { emptyPhysicalItem } from "./module/data/Item/PhysicalItemData";
import { emptyPlayerCharacter } from "./module/data/Actor/PlayerCharacterData";
import { emptyWeapon } from "./module/data/Item/WeaponData";
import { emptyShield } from "./module/data/Item/ShieldData";
import { emptyTalent } from "./module/data/Item/TalentData";
import {
    physicalItemType,
    weaponType,
    shieldType,
    armorType,
    focusType,
    powerType,
    talentType,
} from "./module/data/Item/ItemTypes";

export const Actors = {
    pc: emptyPlayerCharacter,
};

export const Items = {
    [physicalItemType]: emptyPhysicalItem,
    [weaponType]: emptyWeapon,
    [shieldType]: emptyShield,
    [armorType]: emptyArmor,
    [focusType]: emptyFocus,
    [powerType]: powerData,
    [talentType]: emptyTalent,
};
